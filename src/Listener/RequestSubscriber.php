<?php

namespace App\Listener;

use App\Service\LanguageService;
use App\Service\SystemLanguageService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestSubscriber implements EventSubscriberInterface
{
    private SystemLanguageService $systemLanguageService;

    public function __construct(SystemLanguageService $systemLanguageService)
    {
        $this->systemLanguageService = $systemLanguageService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onKernelRequest',
        ];
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        $locale = $request->headers->get('x-locale');
        if(!in_array($locale, $this->systemLanguageService->getLocales())) {
            $locale = 'en';
        } 
        $request->setLocale($locale);
    }
}
