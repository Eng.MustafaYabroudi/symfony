<?php

namespace App\Listener;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Symfony\Component\Security\Core\User\UserInterface;

class JWTListener
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param JWTDecodedEvent $event
     *
     * @return void
     */
    public function onJWTDecoded(JWTDecodedEvent $event)
    {
        $payload = $event->getPayload();
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneByEmail($payload['email']);
        if($user && !$user->getActive()) {
            $event->markAsInvalid();
        }
    }

    public function onJWTCreated(JWTCreatedEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();
        $roles = $user->getRoles();
        if ($user instanceof UserInterface) {
            $payload = $event->getData();
            $payload['userId'] = $user->getId();
            $payload['email'] = $user->getEmail();
            $payload['role'] = count($roles) ? $roles[0] : null;
            $event->setData($payload);
        }
    }

}
