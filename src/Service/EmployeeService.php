<?php

namespace App\Service;

use App\Entity\Employee;
use App\Lib\SearchHelper;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;

class EmployeeService
{

    private EntityManagerInterface $em;
    private PaginatorInterface $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getList($page, $sex, $name): PaginationInterface
    {
        $qb = $this
                ->em
                ->createQueryBuilder()
                ->select('e')
                ->from(Employee::class, 'e')
                ->where('1=1');

        if($sex) {
            $qb
                ->andWhere('e.sex = :sex')
                ->setParameter('sex', $sex);
        }

        if($name) {
            $qb->andWhere(' MATCH (e.name) AGAINST (:name IN BOOLEAN MODE) > 0');
            $name = SearchHelper::stripPunctuation($name);
            $qb->setParameter('name', $name);
        }

        $query = $qb->getQuery();
        return $this->paginator->paginate(
            $query,
            $page,
            10
        );
    }

}