<?php

namespace App\Service;

use App\Entity\SocialStatus;
use App\Entity\Category;
use App\Lib\SearchHelper;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;

class CategoryService
{

    private EntityManagerInterface $em;
    private PaginatorInterface $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getList($page): PaginationInterface
    {
        $qb = $this
                ->em
                ->createQueryBuilder()
                ->select('c')
                ->from(Category::class, 'c');
              

        $query = $qb->getQuery();
        return $this->paginator->paginate(
            $query,
            $page,
            10
        );
    }

}