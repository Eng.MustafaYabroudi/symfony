<?php

namespace App\Service;

use App\Entity\User;
use App\Lib\SearchHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService
{
    private EntityManagerInterface $em;
    private PaginatorInterface $paginator;
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(
        EntityManagerInterface $em,
        PaginatorInterface $paginator,
        UserPasswordHasherInterface $passwordHasher)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->passwordHasher = $passwordHasher;
    }

    public function getList($page, ?array $filter): PaginationInterface
    {
        $filter = new ArrayCollection($filter);

        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('u')
            ->from(User::class, 'u')
            ->where('1 = 1');

        if($filter->get('fullName')) {
            $qb->andWhere(' MATCH (u.fullName) AGAINST (:fullName IN BOOLEAN MODE) > 0');
            $fullName = SearchHelper::stripPunctuation($filter->get('fullName'));
            $qb->setParameter('fullName', $fullName);
        }

        $query = $qb->getQuery();
        return $this->paginator->paginate(
            $query,
            $page,
            10
        );
    }

    public function add(User $user, string $plainPassword): void
    {
        $hashedPassword = $this->passwordHasher->hashPassword($user, $plainPassword);
        $user
            ->setPassword($hashedPassword)
            ->setRegistrationTypeNormal();
        $this->em->persist($user);
        $this->em->flush($user);
    }

    public function changeUserPassword(User $user, $currentPassword, $newPassword): bool
    {
        $currentPasswordValid = $this->passwordHasher->isPasswordValid($user, $currentPassword);
        if($currentPasswordValid) {
            $hashedNewPassword = $this->passwordHasher->hashPassword($user, $newPassword);
            $user->setPassword($hashedNewPassword);
            $this->em->flush();
            return true;
        }
        return false;
    }

}
