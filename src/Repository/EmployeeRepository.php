<?php

namespace App\Repository;

use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EmployeeRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employee::class);
    }

    public function findEmployeesBySex($sex): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('e')
            ->from(Employee::class, 'e')
            ->where('e.sex = :sex')
            ->setParameter('sex', $sex);

        return $qb->getQuery()->getResult();
    }
}
