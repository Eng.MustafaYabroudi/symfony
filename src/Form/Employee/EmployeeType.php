<?php

namespace App\Form\Employee;

use App\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',null,[
                'constraints'=>[
                  
                ]
            ])
            ->add('sex',null,[
                'documentation'=>[
                    'example'=>1
                ]
            ])
            ->add('dateOfBirth',null,[
                'widget'=>'single_text',
                'html5'=>false,
                'format'=>'dd/MM/yyyy',
                'documentation'=>[
                    'example'=>'12/12/2010'
                ]
            ])   
            ->add('active')
            ->add('socialStatus',null,[
                'documentation'=>[
                    'type'=>'integer',
                    'example'=>1

                ]
            ])
            ->add('hobby',null,[
                'documentation'=>[
                    'example'=>[1,2]
                ]
            ]
                
            )
            

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
