<?php

namespace App\Entity;

use App\Entity\Hobby;
use App\Entity\SocialStatus;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;
/**
 * Employee
 *
 * @ORM\Table(name="employee", indexes={@ORM\Index(name="idx_social_status_id", columns={"social_status_id"}), @ORM\Index(name="idx_active", columns={"active"}), @ORM\Index(name="idx_ft_name", columns={"name"}), @ORM\Index(name="idx_created_at", columns={"created_at"}), @ORM\Index(name="idx_updated_at", columns={"updated_at"}), @ORM\Index(name="idx_sex", columns={"sex"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("All")
 */
class Employee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="sex", type="integer", nullable=false)
     * @Serializer\Expose()
     */
    private $sex;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_birth", type="date", nullable=false)
     * @Serializer\Expose()
     * @Serializer\Type("DateTime<'d/m/Y'>")
     * @OA\Property(example="1/1/2001")
     */
    private $dateOfBirth;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Expose()
     * @Serializer\Type("DateTime<'d/m/Y H:i:s'>")
     * @OA\Property(example="1/1/2001")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Serializer\Expose()
     * @Serializer\Type("DateTime<'d/m/Y H:i:s'>")
     * @OA\Property(example="1/1/2001")
     */
    private $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     * @Serializer\Expose()
     */
    private $active;
    

    /**
     * @var \SocialStatus
     *
     * @ORM\ManyToOne(targetEntity="SocialStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="social_status_id", referencedColumnName="id")
     * })
     */
    private $socialStatus;

    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Hobby", inversedBy="employee")
     * @ORM\JoinTable(name="employee_hobby",
     *   joinColumns={
     *     @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="hobby_id", referencedColumnName="id")
     *   }
     * )
     */
    private $hobby = array();

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hobby = new \Doctrine\Common\Collections\ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSex(): ?int
    {
        return $this->sex;
    }

    public function setSex(int $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * @Serializer\Expose()
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("sexString")
     */
    public function getSexText()
    {
        return $this->sex == 1 ? 'Male' : 'Female';
    }

    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
    public function getSocialStatus()
    {
        return $this->socialStatus;
    }

    public function setSocialStatus(?SocialStatus $socialStatus): self
    {
        $this->socialStatus = $socialStatus;

        return $this;
    }

    /**
     * @Serializer\Expose()
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("socialStatusName")
     */
    public function getSocialText()
    {
       if($this->socialStatus){
        return $this->getSocialStatus()->getName();
       }
       return null;
    }
      /**
     * @return Collection<int, Hobby>
     */
    public function getHobby(): Collection
    {
        return $this->hobby;
    }

    public function addHobby(Hobby $hobby): self
    {
        if (!$this->hobby->contains($hobby)) {
            $this->hobby->add($hobby);
        }

        return $this;
    }

    public function removeHobby(Hobby $hobby): self
    {
        $this->hobby->removeElement($hobby);

        return $this;
    }

 /**
     * @Serializer\Expose()
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("HobbiesNames")
     */
    public function getHobbiesNames()
    {
       $out = [];
       /** @var \App\Entity\Hobby $hobby */
       foreach($this->getHobby() as $hobby){
        $out[] = $hobby->getName();
       }
       return $out;
    }

    /**
     * @ORM\PrePersist()
     */
    public function beforeAdd()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function beforeUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
   

}
