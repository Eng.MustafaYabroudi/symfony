<?php

namespace App\Entity;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="idx_facebook_id", columns={"facebook_id"}), @ORM\UniqueConstraint(name="idx_google_id", columns={"google_id"}), @ORM\UniqueConstraint(name="idx_email", columns={"email"})}, indexes={@ORM\Index(name="idx_active", columns={"active"}), @ORM\Index(name="idx_ft_full_name", columns={"full_name"}), @ORM\Index(name="idx_registration_type", columns={"registration_type"})})
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Serializer\ExclusionPolicy("all")
 * @UniqueEntity(
 *      fields={"email"},
 *      message="The email is alreadt used"
 * )
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    const REGISTRATION_TYPE_NORMAL = 1;
    const REGISTRATION_TYPE_FACEBOOK = 2;
    const REGISTRATION_TYPE_GOOGLE = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Serializer\Expose()
     */
    private $email;


    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Serializer\Expose()
     */
    private $fullName;


    /**
     * @var string|null
     *
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    private $facebookId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     */
    private $facebookAccessToken;

    /**
     * @var string|null
     *
     * @ORM\Column(name="google_id", type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="google_access_token", type="string", length=255, nullable=true)
     */
    private $googleAccessToken;

    /**
     * @var int
     *
     * @ORM\Column(name="registration_type", type="integer", nullable=false)
     */
    private $registrationType;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     * @Serializer\Expose()
     */
    private $active;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = strtolower($email);
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): self
    {
        $this->fullName = $fullName;
        return $this;
    }


    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;
        return $this;
    }

    public function getFacebookAccessToken(): ?string
    {
        return $this->facebookAccessToken;
    }

    public function setFacebookAccessToken(?string $facebookAccessToken): self
    {
        $this->facebookAccessToken = $facebookAccessToken;
        return $this;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): self
    {
        $this->googleId = $googleId;
        return $this;
    }

    public function getGoogleAccessToken(): ?string
    {
        return $this->googleAccessToken;
    }

    public function setGoogleAccessToken(?string $googleAccessToken): self
    {
        $this->googleAccessToken = $googleAccessToken;
        return $this;
    }

    public function getRegistrationType(): ?int
    {
        return $this->registrationType;
    }

    public function setRegistrationType(int $registrationType): self
    {
        $this->registrationType = $registrationType;
        return $this;
    }

    public function setRegistrationTypeNormal(): self
    {
        $this->registrationType = self::REGISTRATION_TYPE_NORMAL;
        return $this;
    }

    public function isRegistrationTypeNormal(): bool
    {
        return $this->registrationType == self::REGISTRATION_TYPE_NORMAL;
    }

    public function setRegistrationTypeFacebook(): self
    {
        $this->registrationType = self::REGISTRATION_TYPE_FACEBOOK;
        return $this;
    }

    public function isRegistrationTypeFacebook(): bool
    {
        return $this->registrationType == self::REGISTRATION_TYPE_FACEBOOK;
    }

    public function setRegistrationTypeGoogle(): self
    {
        $this->registrationType = self::REGISTRATION_TYPE_GOOGLE;
        return $this;
    }

    public function isRegistrationTypeGoogle(): bool
    {
        return $this->registrationType == self::REGISTRATION_TYPE_GOOGLE;
    }


    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    public function getRoles(): array
    {
        return [];
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername(): ?string
    {
        return $this->getEmail();
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }

    public function getUserIdentifier(): string
    {
        return $this->getEmail();
    }

}
