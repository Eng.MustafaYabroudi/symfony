<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslationTrait;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * CategoryTranslation
 *
 * @ORM\Table(name="category_translation", uniqueConstraints={@ORM\UniqueConstraint(name="idx_translatable_id", columns={"locale", "translatable_id"})}, indexes={@ORM\Index(name="idx_locale", columns={"locale"}), @ORM\Index(name="translatable_id", columns={"translatable_id"})})
 * @Serializer\ExclusionPolicy("All")
 * @ORM\Entity
 */
class CategoryTranslation implements TranslationInterface
{

    use TranslationTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @Serializer\Expose()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Serializer\Expose()
     * @Assert\NotBlank()
     */
    private $name;

    
    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true, options={"default"="NULL"})
     * @Serializer\Expose()
     */
    private $description = 'NULL';
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


  


}
