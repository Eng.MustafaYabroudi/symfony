<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Category
 *
 * @ORM\Table(name="category", indexes={@ORM\Index(name="idx_active", columns={"active"})})
 * @Serializer\ExclusionPolicy("All")
 * @Serializer\VirtualProperty(
 * "translation",
 * exp = "object.getTranslations()",
 * options ={@Serializer\SerializedName("Translations")}
 * )
 * @ORM\Entity
 */
class Category implements TranslatableInterface
{
    use TranslatableTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     * @Serializer\Expose()
     */
    private $active;

    /** 
     * @Assert\Valid()
     */
    Protected $translations;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }


}
