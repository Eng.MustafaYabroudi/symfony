<?php

namespace App\Controller\Api;

use App\Entity\SocialStatus;
use App\Service\RestHelperService;
use App\Service\SocialStatusService;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\SocialStatus\SocialStatusType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Nelmio\ApiDocBundle\Annotation\Security;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

#[Route('/api')]
class SocialStatusController extends AbstractFOSRestController
{
    private EntityManagerInterface $em;
    private RestHelperService $rest;
    private SocialStatusService $socialStatusService;
    public function __construct
    (
        EntityManagerInterface $em,
        RestHelperService $rest,
        SocialStatusService $socialStatusService
    ) {
        $this->em = $em;
        $this->rest = $rest;
        $this->socialStatusService = $socialStatusService;

    }
    #[Route('/socialStatus', methods: ['GET'])]
     /**
     *  @Security(name="Bearer")
     * @OA\Tag(name="socialStatus")
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     example=1,
     *     description="Page number",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="sort",
     *     in="query",
     *     example="s.id",
     *     description="Order By",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="direction",
     *     in="query",
     *     description="Order By Direction",
     *     @OA\Schema(type="string",enum ={"desc","asc"},default ="asc")
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns sociaStatus",
     *   @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="pagination",type="object",
     *             @OA\Property(property="page",type="integer"),
     *             @OA\Property(property="pages",type="integer"),
     *             @OA\Property(property="titleItems",type="integer"),
     *             @OA\Property(property="items",type="array",
     *                 @OA\Items(ref=@Model(type=socialStatus::class))
     *                    )
     *             )
     *     )
     * 
     * )
     */
    public function list(Request $request): Response
    {
        $page = $request->query->get('page', 1);
        $list = $this->socialStatusService->getList($page);
        $this->rest->succeeded()->setPagination($list);
        return $this->handleView(
            $this->view($this->rest->getResponse())
        );

    }
    #[Route('/socialStatus', methods: ['POST'])]
     /**
     * @Security(name="Bearer")
     * @OA\Tag(name="socialStatus")
     * @OA\RequestBody(
     *      description="Add new socialStatus",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=socialStatusType::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns the Student",
     *     @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="data",type="object",ref=@Model(type=socialStatus::class)),
     *     )
     * )
     */
    public function add(Request $request): Response
    {
        $socialStatus = new SocialStatus();

        $form = $this->createForm(SocialStatusType::class, $socialStatus);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($socialStatus);
            $this->em->flush();
            $this->rest->succeeded()->setCustom('socicalStatus', $socialStatus);
            return $this->handleView($this->view($this->rest->getResponse()));
        }

        $this->rest->failed()->setFormErrors($form->getErrors());

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );

    }

    #[Route('/socialStatus/{id}', methods: ['PUT'], requirements: ['id' => '\d+'])]
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="socialStatus")
     * @OA\RequestBody(
     *      description="Edit socialStatus",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=socialStatusType::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns the Student",
     *     @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="data",type="object",ref=@Model(type=socialStatus::class)),
     *     )
     * )
     */
    public function update(Request $request, SocialStatus $socialStatus): Response
    {
        $form = $this->createForm(SocialStatusType::class, $socialStatus);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->rest->succeeded()->setCustom('socialStatuse', $socialStatus);
            return $this->handleView(
                $this->view($this->rest->getResponse(), Response::HTTP_CREATED)
            );
        }

        $this->rest->failed()->setFormErrors($form->getErrors());

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );

    }

    #[Route('/socialStatus/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="socialStatus")
     */
    public function delete(SocialStatus $socialStatus): Response
    {
        try {
            $this->em->remove($socialStatus);
            $this->em->flush();

            $this->rest->succeeded()->setCustom('socialStatus', $socialStatus);
        }catch(ForeignKeyConstraintViolationException $e){
            $this->rest->failed()->setCustom(
                'Erorr',
                'Unable to remove the social Status because it might be used with other data'
            );
        }

        return $this->handleView(
            $this->view($this->rest->getResponse())

        );

    }

    #[Route('/socialStatus/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
     /**
     * @Security(name="Bearer")
     * @OA\Tag(name="socialStatus")
     */
    public function getSocialStatus(SocialStatus $socialStatus)
    {
        $this->rest->succeeded()->setCustom('socialStatus', $socialStatus);
        return $this->handleView($this->view($this->rest->getResponse()));

    }


}