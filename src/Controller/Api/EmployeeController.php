<?php

namespace App\Controller\Api;

use App\Entity\Employee;
use App\Service\EmployeeService;
use App\Service\RestHelperService;
use App\Form\Employee\EmployeeType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Security;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Translation\LocaleSwitcher;

#[Route('/api')]
class EmployeeController extends AbstractFOSRestController
{

    private EntityManagerInterface $em;
    private RestHelperService $rest;
    private EmployeeService $employeeService;

    public function __construct(
        EntityManagerInterface $em,
        RestHelperService $rest,
        EmployeeService $employeeService
    ) {
        $this->em = $em;
        $this->rest = $rest;
        $this->employeeService = $employeeService;
    }
      
    #[Route('/employee', methods: ['GET'])]
     /**
     *  @Security(name="Bearer")
     * @OA\Tag(name="Employee")
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     example=1,
     *     description="Page number",
     *     @OA\Schema(type="integer")
     * )
     * * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     example="name",
     *     description="Name ",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="sex",
     *     in="query",
     *     example=1,
     *     description="Sex",
     *     @OA\Schema(type="integer",enum={1,2})
     * )
     * @OA\Parameter(
     *     name="sort",
     *     in="query",
     *     example="e.id",
     *     description="Order By",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="direction",
     *     in="query",
     *     description="Order By Direction",
     *     @OA\Schema(type="string",enum ={"desc","asc"},default ="asc")
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns Employee list",
     *     @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="pagination",type="object",
     *             @OA\Property(property="page",type="integer"),
     *             @OA\Property(property="pages",type="integer"),
     *             @OA\Property(property="titleItems",type="integer"),
     *             @OA\Property(property="items",type="array",
     *                 @OA\Items(ref=@Model(type=Employee::class))
     *                    )
     *             )
     *     )
     * )
     */
    
    public function list(Request $request): Response
    {
        $page = $request->query->get('page', 1);
        $sex = $request->query->get('sex');
        $name = $request->query->get('name');
        $list = $this->employeeService->getList($page, $sex, $name);

        $this->rest->succeeded()->setPagination($list);

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }
    #[Route('/employee', methods: ['POST'])]
     /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Employee")
     * @OA\RequestBody(
     *      description="Add new Employee",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=EmployeeType::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns the Student",
     *     @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="data",type="object",ref=@Model(type=Employee::class)),
     *     )
     * )
     */
    public function add(Request $request): Response
    {
        $employee = new Employee();
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($employee);
            $this->em->flush();
            $this->rest->succeeded()->setCustom('employee', $employee);
        }

        $this->rest->failed()->setFormErrors($form->getErrors());

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }

    #[Route('/employee/{id}', methods: ['PUT'], requirements: ['id' => '\d+'])]
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Employee")
     * @OA\RequestBody(
     *      description="Edit Employee",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=EmployeeType::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns the Student",
     *     @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="data",type="object",ref=@Model(type=Employee::class)),
     *     )
     * )
     */
    public function update(Request $request,LocaleSwitcher $localeSwitcher, Employee $employee): Response
    {  
        $localeSwitcher->setLocale(LC_ALL,'ar');
        $form = $this->createForm(EmployeeType::class, $employee); 
        $form->submit($request->request->all());    
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->rest->succeeded()->setCustom('employee', $employee);
            return $this->handleView($this->view($this->rest->getResponse(), Response::HTTP_CREATED));
        }

        $this->rest->failed()->setFormErrors($form->getErrors());

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }

    #[Route('/employee/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    /**
     * @OA\Tag(name="Employee")
     * @Security(name="Bearer")
     */
    public function delete(Employee $employee): Response
    {
        $this->em->remove($employee);
        $this->em->flush();
        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }
    #[Route('/employee/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
      /**
     * @OA\Tag(name="Employee")
     * @Security(name="Bearer")
     */
    public function getEmployee($id)
    {   $employee = $this->em->getRepository(Employee::class)->find($id);
        $this->rest->succeeded()->setCustom('employee', $employee);
        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
        
    }
}