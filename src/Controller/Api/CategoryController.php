<?php

namespace App\Controller\Api;

use App\Entity\Category;
use OpenApi\Annotations as OA;
use App\Service\CategoryService;
use App\Service\RestHelperService;
use App\Entity\CategoryTranslation;
use App\Form\Category\CategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use JMS\Serializer\Annotation as Serializer;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

#[Route('/api')]
class CategoryController extends AbstractFOSRestController
{
    private EntityManagerInterface $em;
    private RestHelperService $rest;
    private CategoryService $categoryService;
    public function __construct
    (
        EntityManagerInterface $em,
        RestHelperService $rest,
        CategoryService $categoryService
    ) {
        $this->em = $em;
        $this->rest = $rest;
        $this->categoryService = $categoryService;

    }
    #[Route('/category', methods: ['GET'])]
    /**
     *  @Security(name="Bearer")
     * @OA\Tag(name="Category")
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     example=1,
     *     description="Page number",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="sort",
     *     in="query",
     *     example="c.id",
     *     description="Order By",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="direction",
     *     in="query",
     *     description="Order By Direction",
     *     @OA\Schema(type="string",enum ={"desc","asc"},default ="asc")
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns sociaStatus",
     *   @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="pagination",type="object",
     *             @OA\Property(property="page",type="integer"),
     *             @OA\Property(property="pages",type="integer"),
     *             @OA\Property(property="titleItems",type="integer"),
     *             @OA\Property(property="items",type="array",
     *                 @OA\Items(ref=@Model(type=CategoryTranslation::class))
     *                    )
     *             )
     *     )
     * 
     * )
     */
    public function list(Request $request): Response
    {
        $page = $request->query->get('page', 1);
        $list = $this->categoryService->getList($page);
        $this->rest->succeeded()->setPagination($list);
        return $this->handleView(
            $this->view($this->rest->getResponse())
        );

    }
    #[Route('/category', methods: ['POST'])]
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Category")
     * @OA\RequestBody(
     *      description="Add new Category",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=CategoryTranslation::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns the Student",
     *     @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="data",type="object",ref=@Model(type=CategoryTranslation::class)),
     *     )
     * )
     */
   
    public function add(Request $request): Response
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($category);
            $this->em->flush();
            $this->rest->succeeded()->setCustom('category', $category);
            return $this->handleView($this->view($this->rest->getResponse()));
        }

        $this->rest->failed()->setFormErrors($form->getErrors());

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );

    }

    #[Route('/category/{id}', methods: ['PUT'], requirements: ['id' => '\d+'])]
    /**
     * @Security(name="Bearer")
     * @OA\Tag(name="Category")
     * @OA\RequestBody(
     *      description="Edit Category",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=CategoryTranslation::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns the Student",
     *     @OA\JsonContent(
     *         @OA\Property(property="success",type="boolean"),
     *         @OA\Property(property="data",type="object",ref=@Model(type=CategoryTranslation::class)),
     *     )
     * )
     */
   
    public function update(Request $request, Category $category): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->rest->succeeded()->setCustom('category', $category);
            return $this->handleView(
                $this->view($this->rest->getResponse(), Response::HTTP_CREATED)
            );
        }

        $this->rest->failed()->setFormErrors($form->getErrors());

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );

    }

    #[Route('/category/{id}', methods: ['DELETE'], requirements: ['id' => '\d+'])]
    /**
     * @OA\Tag(name="Category")
     * @Security(name="Bearer")
     */
    public function delete(Category $category): Response
    {
        try {
            $this->em->remove($category);
            $this->em->flush();

            $this->rest->succeeded()->setCustom('socialStatus', $category);
        }catch(ForeignKeyConstraintViolationException $e){
            $this->rest->failed()->setCustom(
                'Erorr',
                'Unable to remove the social Status because it might be used with other data'
            );
        }

        return $this->handleView(
            $this->view($this->rest->getResponse())

        );

    }

    #[Route('/category/{id}', methods: ['GET'], requirements: ['id' => '\d+'])]
    /**
     * @OA\Tag(name="Category")
     * @Security(name="Bearer")
     */
    public function getCategory(Category $category)
    {
        $this->rest->succeeded()->setCustom('category', $category);
        return $this->handleView($this->view($this->rest->getResponse()));

    }


}