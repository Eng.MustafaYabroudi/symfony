<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Form\User\ChangeMyPasswordType;
use App\Form\User\UpdateMyProfileType;
use App\Form\User\UserType;
use App\Service\RestHelperService;
use App\Service\UserService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Security;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;


/**
 * @Rest\Route("/api/user")
 */
class UserController extends AbstractFOSRestController
{

    private EntityManagerInterface $em;
    private RestHelperService $rest;
    private UserService $userService;

    public function __construct(EntityManagerInterface $em, RestHelperService $rest, UserService $userService)
    {
        $this->em = $em;
        $this->rest = $rest;
        $this->userService = $userService;
    }


   /**
     * Lists all Users.
     * @Rest\Post("")
     * @OA\Response(
     *     response=200,
     *     description="Returns the Users"
     * )
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     example=1,
     *     description="Page number",
     *     @OA\Schema(type="integer")
     * )
     * @Security(name="Bearer")
     * @OA\Tag(name="User")
     */ 
    public function listUsers(Request $request): Response
    {
        $input = new ArrayCollection($request->request->all());
        $page = $request->query->getInt('page', 1);
        $filter = (array) $input->get('filter');
        $pagination = $this->userService->getList($page, $filter);
        $this
            ->rest
            ->setPagination($pagination);

        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }


    /**
     * Get User.
     * @Rest\Get("/{id}")
     * @OA\Tag(name="User")
     * @param User $user
     * @return Response
     */
    public function getUserData(User $user): Response
    {
        return $this->handleView($this->view($user));
    }


    /**
     * Create User.
     * @Rest\Post("")
     * @OA\RequestBody(
     *      description="Add new User",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=User::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Tag(name="User")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $user = new User();
        $user->setPassword(Uuid::uuid4());
        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->userService->add(
                $user,
                $form->get('password')->getData()
            );
            $this->em->persist($user);
            $this->em->flush();
            $this->rest->setData($user);
            return $this->handleView(
                $this->view($user, Response::HTTP_CREATED)
            );
        }
        $this->rest->failed()->setFormErrors($form->getErrors());
        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }

    /**
     * Update User.
     * @Rest\Put("/{id}")
     * @OA\RequestBody(
     *      description="Edit User",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=User::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Tag(name="User")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function update(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->rest->setData($user);
            return $this->handleView(
                $this->view($this->rest->getResponse(), Response::HTTP_CREATED)
            );
        }
        $this->rest->failed()->setFormErrors($form->getErrors());
        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }


    /**
     * Delete User.
     * @Rest\Delete("/{id}")
     * @OA\Tag(name="User")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function delete(Request $request, User $user): Response
    {
        try {
            $this->em->remove($user);
            $this->em->flush();

            $this->rest->setData($user);
        }catch (ForeignKeyConstraintViolationException $e) {
            $this->rest->failed()->addMessage('Unable to remove the user, it might be used with other data');
        }
        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }




    /**
     * Update Profile.
     * @Rest\Post("/update-profile")
     * @OA\RequestBody(
     *      description="Add new User",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=Genre::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Tag(name="User")
     * @param Request $request
     * @return Response
     */
    public function updateProfile(Request $request): Response
    {

        dump($request->files->get('image'));exit;

//        $form = $this->createForm(GenreType::class, );
//        $form->submit($request->request->all());
//        if ($form->isSubmitted() && $form->isValid()) {
//            $this->em->persist($genre);
//            $this->em->flush();
//            $this->rest->setData($genre);
//            return $this->handleView(
//                $this->view($genre, Response::HTTP_CREATED)
//            );
//        }
//        $this->rest->failed()->setFormErrors($form->getErrors());
        return $this->handleView($this->view($this->rest->getResponse()));
    }


    /**
     * Get Profile Data.
     * @Rest\Get("/profile/get-profile-data")
     * @OA\Tag(name="User")
     * @return Response
     */
    public function getProfileData(): Response
    {
        $user = $this->getUser();
        return $this->handleView($this->view($user));
    }


    /**
     * Change my password.
     * @Rest\Post("/profile/change-password")
     * @OA\RequestBody(
     *      description="Change My Password",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=Genre::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Tag(name="User")
     * @param Request $request
     * @param JWTTokenManagerInterface $JWTManager
     * @return Response
     * @throws Exception
     */
    public function changeMyPassword(Request $request, JWTTokenManagerInterface $JWTManager): Response
    {
        $me = $this->getUser();
        $form = $this->createForm(ChangeMyPasswordType::class);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $passChanged = $this->userService->changeUserPassword(
                $me,
                $form->get('currentPassword')->getData(),
                $form->get('newPassword')->getData()
            );
            if(!$passChanged) {
                $form->get('currentPassword')->addError(new FormError('The current password is incorrect'));
                $this->rest->failed()->setFormErrors($form->getErrors());
            }else{
                $this->rest->setCustom('token', $JWTManager->create($me));
            }
            return $this->handleView($this->view($this->rest->getResponse()));
        }
        $this->rest->failed()->setFormErrors($form->getErrors());
        return $this->handleView($this->view($this->rest->getResponse()));
    }


    /**
     * Update my profile.
     * @Rest\Post("/profile/edit-profile")
     * @OA\RequestBody(
     *      description="Update My Profile",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              ref=@Model(type=Genre::class, groups={"Default"})
     *          )
     *      )
     * )
     * @OA\Tag(name="User")
     * @param Request $request
     * @param JWTTokenManagerInterface $JWTManager
     * @return Response
     * @throws Exception
     */
    public function updateMyProfile(Request $request, JWTTokenManagerInterface $JWTManager): Response
    {
        $me = $this->getUser();
        $form = $this->createForm(UpdateMyProfileType::class, $me);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->em->refresh($me);
            $this->rest->setCustom('token', $JWTManager->create($me));
            return $this->handleView($this->view($this->rest->getResponse()));
        }
        $this->rest->failed()->setFormErrors($form->getErrors());
        return $this->handleView($this->view($this->rest->getResponse()));
    }

}
