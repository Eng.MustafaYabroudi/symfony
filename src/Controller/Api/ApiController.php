<?php

namespace App\Controller\Api;

use App\Service\LanguageService;
use App\Service\RestHelperService;
use App\Service\SystemLanguageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class ApiController extends AbstractFOSRestController
{

    private SystemLanguageService $systemLanguageService;
    private RestHelperService $rest;

    public function __construct(SystemLanguageService $systemLanguageService, RestHelperService $restHelperService)
    {
        $this->systemLanguageService = $systemLanguageService;
        $this->rest = $restHelperService;
    }

    #[Route('/get-languages')]
    public function getLanguages(): Response
    {
        $list = $this->systemLanguageService->getLanguages();
        $this->rest->setData($list);
        return $this->handleView(
            $this->view($this->rest->getResponse())
        );
    }
}
