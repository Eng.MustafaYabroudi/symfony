<?php

namespace App\Controller;

use App\Entity\Employee;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
//    #[Route('/', name: 'app_main')]
//    #[Route('/{route}', name: 'portal_vue_pages', requirements: ["route" => "^(?!.*_wdt|_wdt|_profiler|api|img).+"])]
//    public function index(): Response
//    {
//        return $this->render('main/index.html.twig');
//    }

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    #[Route('/', name: 'app_main')]
    public function index(): Response
    {

        $employeeRepo = $this->em->getRepository(Employee::class);

        /** @var Employee $employee */
        $employee = $employeeRepo->find(5);

        // $this->em->remove($employee);
        // $this->em->flush();

//        $employee = new Employee();
//        $employee
//            ->setName('Mohammed Ahmed Mohammed')
//            ->setSex(1)
//            ->setActive(1)
//            ->setDateOfBirth(new \DateTime('2015-09-31'));
//
//        $this->em->persist($employee);
//        $this->em->flush($employee);

        return $this->render('main/index.html.twig', [
            'employee' => $employee,
        ]);
    }
}
