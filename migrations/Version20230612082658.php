<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230612082658 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
       $this->addSql("CREATE TABLE `employee_hobby` (
                            `employee_id` int(11) NOT NULL AUTO_INCREMENT,
                            `hobby_id` int(11) NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;");

       $this->addSql("ALTER TABLE `employee_hobby`
                            ADD PRIMARY KEY (`employee_id`,`hobby_id`),
                            ADD KEY `idx_hobby_id` (`hobby_id`) USING BTREE;
                            ADD CONSTRAINT `employee_hobby_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
                            ADD CONSTRAINT `employee_hobby_ibfk_2` FOREIGN KEY (`hobby_id`) REFERENCES `hobby` (`id`);");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
