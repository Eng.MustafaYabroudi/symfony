<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221010144552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("CREATE TABLE IF NOT EXISTS `employee` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                            `sex` int NOT NULL,
                            `date_of_birth` date NOT NULL,
                            `active` tinyint(1) NOT NULL,
                            `social_status_id` int(11) DEFAULT NULL,
                            PRIMARY KEY (`id`),
                            KEY `idx_sex` (`sex`),
                            KEY `idx_active` (`active`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        $this->addSql("ALTER TABLE `employee` 
                            ADD `created_at` DATETIME NOT NULL AFTER `date_of_birth`, 
                            ADD `updated_at` DATETIME NOT NULL AFTER `created_at`, 
                            ADD INDEX `idx_created_at` (`created_at`), 
                            ADD INDEX `idx_updated_at` (`updated_at`);
                            ADD KEY `idx_social_status_id` (`social_status_id`) USING BTREE;
                            ADD FULLTEXT KEY `idx_ft_name` (`name`);");
                               
}

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}