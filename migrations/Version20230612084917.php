<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230612084917 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("CREATE TABLE `category_translation` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `name` varchar(255) NOT NULL,
                            `description` text DEFAULT NULL,
                            `locale` varchar(2) NOT NULL,
                            `translatable_id` int(11) NOT NULL,
                            PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;");

        $this->addSql("ALTER TABLE `category_translation`
                            ADD UNIQUE KEY `idx_translatable_id` (`locale`,`translatable_id`) USING BTREE,
                            ADD KEY `idx_locale` (`locale`) USING BTREE,
                            ADD KEY `translatable_id` (`translatable_id`);
                            ADD CONSTRAINT `category_translation_ibfk_1` FOREIGN KEY (`translatable_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
