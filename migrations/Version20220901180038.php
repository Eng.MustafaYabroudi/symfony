<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220901180038 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE IF NOT EXISTS `user` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `full_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                          `facebook_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                          `facebook_access_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                          `google_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                          `google_access_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                          `registration_type` int NOT NULL,
                          `active` tinyint(1) NOT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `idx_email` (`email`) USING BTREE,
                          UNIQUE KEY `idx_facebook_id` (`facebook_id`),
                          UNIQUE KEY `idx_google_id` (`google_id`),
                          KEY `idx_active` (`active`) USING BTREE,
                          KEY `idx_registration_type` (`registration_type`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        $this->addSql("ALTER TABLE `user` ADD FULLTEXT KEY `idx_ft_full_name` (`full_name`);");
        $this->addSql("INSERT INTO `user` (`id`, `email`, `password`, `full_name`, `facebook_id`, `facebook_access_token`, `google_id`, `google_access_token`, `registration_type`, `active`) VALUES
                            (1, 'admin@admin.com', '". '$2y$10$gkiuDf/9Lyym/EhqxzTVoepZf45jpLi1ak9cMr3DEWWtz10q8c6dS' ."', 'admin4553453', NULL, NULL, NULL, NULL, 1, 1);");
        $this->addSql("CREATE TABLE IF NOT EXISTS `messenger_message` (
                              `id` bigint NOT NULL AUTO_INCREMENT,
                              `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                              `headers` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                              `queue_name` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
                              `created_at` datetime NOT NULL,
                              `available_at` datetime NOT NULL,
                              `delivered_at` datetime DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `IDX_8F9F0C97FB7336F0` (`queue_name`),
                              KEY `IDX_8F9F0C97E3BD61CE` (`available_at`),
                              KEY `IDX_8F9F0C9716BA31DB` (`delivered_at`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
